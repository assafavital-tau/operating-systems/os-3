#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <stdbool.h>

void print_error(const char *msg)
{
	printf("[ERROR] %s: %s.\n", msg, strerror(errno));
}

/* ------- TYPEDEFS ------- */

typedef struct intnode
{
	int value;
	struct intnode *prev, *next;
} *Node;

typedef struct intlist
{
	Node head, tail;
	int current_size;
	pthread_mutex_t mutex;
	pthread_cond_t poppable;
} *List;

/* ------- INTLIST FUNCTIONS ------- */

void intlist_init(List list)
{
	// Assign primitive attributes
	list->head = NULL;
	list->tail = NULL;
	list->current_size = 0;

	// Initialize mutex variables
	int rc;
	pthread_mutexattr_t attr;
	rc = pthread_mutexattr_init(&attr);
	if(rc)
	{
		printf("Failed to initialize mutex attribute\n");
		free(list);
		exit(rc);
	}
	rc = pthread_mutexattr_settype(&attr,PTHREAD_MUTEX_RECURSIVE);
	if(rc)
	{
		printf("Failed to set mutex attribute type\n");
		free(list);
		pthread_mutexattr_destroy(&attr);
		exit(rc);
	}
	rc = pthread_mutex_init(&list->mutex, &attr);
	if(rc)
	{
		printf("Failed to initialize mutex\n");
		free(list);
		pthread_mutexattr_destroy(&attr);
		exit(rc);
	}
	pthread_mutexattr_destroy(&attr);
	rc = pthread_cond_init(&list->poppable, NULL);
	if(rc)
	{
		printf("Failed to initialize conditional variable\n");
		free(list);
		pthread_mutex_destroy(&list->mutex);
		exit(rc);
	}
}

pthread_mutex_t * intlist_get_mutex(List list)
{
	return &list->mutex;
}

int intlist_size(List list)
{
	pthread_mutex_t *mtx = intlist_get_mutex(list);
	int rc = pthread_mutex_lock(mtx);
	if(rc)
	{
		printf("Mutex lock failed\n");
		exit(rc);
	}
	int size = list->current_size;
	rc = pthread_mutex_unlock(mtx);
	if(rc)
	{
		printf("Mutex unlock failed\n");
		exit(rc);
	}
	return size;
}

void intlist_push_head(List list, int value)
{
	// Allocate memory for new node
	Node new_element = (Node) malloc(sizeof(struct intnode));
	if(new_element == NULL)
	{
		print_error("Allocation failed");
		pthread_exit(NULL);
	}
	new_element->value = value;
	new_element->prev = NULL;
	new_element->next = NULL;

	// Critical point: Connect new node from both sides, update list size
	pthread_mutex_t *mtx = intlist_get_mutex(list);
	int rc = pthread_mutex_lock(mtx);
	if(rc)
	{
		printf("Mutex lock failed\n");
		exit(rc);
	}
	Node current_head = list->head;
	new_element->next = current_head;
	list->head = new_element;
	if(current_head == NULL)
		list->tail = new_element;
	else
		current_head->prev = new_element;
	list->current_size++;
	rc = pthread_cond_signal(&list->poppable);
	if(rc)
	{
		printf("Cond. signal failed\n");
		exit(rc);
	}
	rc = pthread_mutex_unlock(mtx);
	if(rc)
	{
		printf("Mutex unlock failed\n");
		exit(rc);
	}
}

int intlist_pop_tail(List list)
{
	// Critical point: Fetch list tail and disconnect, update list size
	pthread_mutex_t *mtx = intlist_get_mutex(list);
	int rc = pthread_mutex_lock(mtx);
	if(rc)
	{
		printf("Mutex lock failed\n");
		exit(rc);
	}
	Node poped;
	while(intlist_size(list) == 0)
	{
		rc = pthread_cond_wait(&list->poppable, mtx);
		if(rc)
		{
			printf("Cond. wait failed\n");
			exit(rc);
		}
	}
	poped = list->tail;
	list->tail = poped->prev;
	if(list->tail)
		list->tail->next = NULL;
	else
		list->head = NULL;
	list->current_size--;
	rc = pthread_mutex_unlock(mtx);
	if(rc)
	{
		printf("Mutex unlock failed\n");
		exit(rc);
	}

	// Get node value, free memory
	int retval = poped->value;
	free(poped);
	return retval;
}

void intlist_remove_last_k(List list, int k)
{
	if(k <= 0) return;
	pthread_mutex_t *mtx = intlist_get_mutex(list);
	Node n;
	int i;
	int rc = pthread_mutex_lock(mtx);
	if(rc)
	{
		printf("Mutex lock failed\n");
		exit(rc);
	}
	int size = intlist_size(list);
	if(k >= size)
	{
		// Clear list
		n = list->head;
		list->head = NULL;
		list->tail = NULL;
		list->current_size = 0;
	}
	else
	{
		// Update list tail
		for(i = 0; i < k; i++)
		{
			list->tail = list->tail->prev;
			list->current_size--;
		}

		n = list->tail->next;
		list->tail->next = NULL;
	}
	rc = pthread_mutex_unlock(mtx);
	if(rc)
	{
		printf("Mutex unlock failed\n");
		exit(rc);
	}

	// Free memory
	while(n->next)
	{
		n = n->next;
		free(n->prev);
	}
	free(n);
}

void intlist_destroy(List list)
{
	// Empty the list
	while(intlist_size(list) > 0)
		intlist_pop_tail(list);

	// Destroy mutex and conditional variable
	pthread_mutex_destroy(intlist_get_mutex(list));
	pthread_cond_destroy(&list->poppable);
}

List glob_list;
pthread_cond_t is_full;
int max_size;
bool end_r, end_w, end_gc;

/* ------- WRITER THREAD ------- */

void signal_gc()
{
	pthread_mutex_t *mtx = intlist_get_mutex(glob_list);
	int rc = pthread_mutex_lock(mtx);
	if(rc)
	{
		printf("Mutex lock failed\n");
		exit(rc);
	}
	if(intlist_size(glob_list) > max_size)
	{
		rc = pthread_cond_signal(&is_full);
		if(rc)
		{
			printf("Cond. signal failed\n");
			exit(rc);
		}
	}
	rc = pthread_mutex_unlock(mtx);
	if(rc)
	{
		printf("Mutex unlock failed\n");
		exit(rc);
	}
}

void * thread_write(void *t)
{
	srand(time(NULL));
	while(!end_w)
	{
		intlist_push_head(glob_list, rand());
		signal_gc(); // Signal GC thread if needed
	}

	pthread_exit(t);
}

/* ------- READER THREAD ------- */

void * thread_read(void *t)
{
	while(!end_r)
	{
		intlist_pop_tail(glob_list);
	}

	pthread_exit(t);
}

/* ------- GARBAGE COLLECTOR THREAD ------- */

void * thread_gc(void *t)
{
	pthread_mutex_t *mtx = intlist_get_mutex(glob_list);
	int rc;
	while(!end_gc)
	{
		rc = pthread_mutex_lock(mtx);
		if(rc)
		{
			printf("Mutex lock failed\n");
			exit(rc);
		}
		while(intlist_size(glob_list) <= max_size)
		{
			rc = pthread_cond_wait(&is_full, mtx);
			if(rc)
			{
				printf("Cond. wait failed\n");
				exit(rc);
			}	
		}
		int k = intlist_size(glob_list)/2;
		intlist_remove_last_k(glob_list, k);
		rc = pthread_mutex_unlock(mtx);
		if(rc)
		{
			printf("Mutex unlock failed\n");
			exit(rc);
		}
		printf("GC - %d items removed from the list\n", k);
	}

	pthread_exit(t);
}

/* ------- SIMULATION ------- */

void print_list()
{
	// Print size
	int size = intlist_size(glob_list);
	printf("List size: %d\n", size);

	// Print items
	Node current = glob_list->head;
	int i = 1;
	while(current)
	{
		printf("Item #%d: %d\n", i, current->value);
		i++;
		current = current->next;
	}
}

int main(int argc, char **argv)
{
	// Check command-line arguments validity; Assign
	if(argc != 5)
	{
		printf("Invalid command-line arguments.\n");
		exit(-1);
	}
	int wnum = strtol(argv[1], NULL, 10);
	int rnum = strtol(argv[2], NULL, 10);
	max_size = strtol(argv[3], NULL, 10);
	int time = strtol(argv[4], NULL, 10);

	end_r = false;
	end_w = false;
	end_gc = false;

	// List initialization
	glob_list = (List) malloc(sizeof(struct intlist));
	if(glob_list == NULL)
	{
		print_error("Allocation failed");
		exit(errno);
	}
	intlist_init(glob_list);

	// Create GC thread
	pthread_t gc_t;
	int rc = pthread_create(&gc_t, NULL, thread_gc, NULL);
	if(rc)
	{
		printf("Failed to create GC thread\n");
		free(glob_list);
		exit(rc);
	}

	// Create writer threads
	pthread_t wr_t[wnum];
	int i;
	for(i = 0; i < wnum; i++)
	{
		rc = pthread_create(&wr_t[i], NULL, thread_write, NULL);
		if(rc)
		{
			printf("Failed to create writer thread #%d\n", i);
			exit(rc);
		}
	}

	// Create reader threads
	pthread_t rd_t[rnum];
	for(i = 0; i < rnum; i++)
	{
		rc = pthread_create(&rd_t[i], NULL, thread_read, NULL);
		if(rc)
		{
			printf("Failed to create reader thread #%d\n", i);
			exit(rc);
		}
	}

	// Sleep
	sleep(time);

	// Join all threads
	void *status;
	end_gc = true;
	rc = pthread_join(gc_t, &status);
	if(rc)
	{
			printf("Failed to join GC thread\n");
			exit(rc);
	}

	end_r = true;
	for(i = 0; i < wnum; i++)
	{
		rc = pthread_join(rd_t[i], &status);
		if(rc)
		{
			printf("Failed to join reader thread #%d\n", i);
			exit(rc);
		}
	}

	end_w = true;
	for(i = 0; i < wnum; i++)
	{
		rc = pthread_join(wr_t[i], &status);
		if(rc)
		{
			printf("Failed to join writer thread #%d\n", i);
			exit(rc);
		}
	}

	// Print list
	print_list();

	// Cleanup
	intlist_destroy(glob_list);
	free(glob_list);

	exit(0);
}